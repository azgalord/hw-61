import React, { Component } from 'react';
import Countries from "./components/Countries/Countries";
import CountryInfo from "./components/CountryInfo/CountryInfo";

import './App.css';

class App extends Component {
  state ={
    alpha3Code: null
  };

  getAlphaCode = (alpha3Code) => {
    this.setState({alpha3Code});
  };

  render() {
    return (
      <div className="App">
        <Countries clicked={this.getAlphaCode}/>
        <CountryInfo alpha3Code={this.state.alpha3Code}/>
      </div>
    );
  }
}

export default App;

import React from 'react';

import './MainCountryInfo.css';

const MainCountryInfo = props => {
  return (
    <div className="MainCountryInfo">
      <h2>{props.title}</h2>
      <p><b>Capital:</b> {props.capital}</p>
      <p><b>Population:</b> {props.population}</p>
      <p><b>Subregion:</b> {props.subregion}</p>
      <p><b>Demonym:</b> {props.demonym}</p>
    </div>
  );
};

export default MainCountryInfo;

import React, {Component, Fragment} from 'react';
import axios from 'axios';

import './CountryInfo.css';
import MainCountryInfo from "./MainCountryInfo/MainCountryInfo";
import CountryFlag from "./CountryFlag/CountryFlag";
import CountryBorders from "./CountryBorders/CountryBorders";

class CountryInfo extends Component {
  state = {
    name: '',
    capital: '',
    subregion: '',
    demonym: '',
    population: '',
    flag: '',
    borders: []
  };

  componentDidUpdate(prevProps) {
    if (this.props.alpha3Code) {
      if(prevProps.alpha3Code !== this.props.alpha3Code) {
        axios.get('/alpha/' + this.props.alpha3Code)
          .then(response => {

            this.setState({
              name: response.data.name,
              capital: response.data.capital,
              subregion: response.data.subregion,
              demonym: response.data.demonym,
              population: response.data.population,
              flag: response.data.flag
            });

            Promise.all(response.data.borders.map(border => (
              axios.get('/alpha/' + border)
                .then(response => {
                  return response.data.name;
                })
            ))).then(borders => {
              this.setState({borders});
            });
          })
      }
    }
  }

  render() {
    return (
      <div className="CountryInfo">
        {this.props.alpha3Code ?
          <Fragment>
            <div className="TopInfo">
              <MainCountryInfo
                title={this.state.name}
                capital={this.state.capital}
                population={this.state.population}
                demonym={this.state.demonym}
                subregion={this.state.subregion}
              />
              <CountryFlag
                background={this.state.flag}
              />
            </div>
            <CountryBorders
              borders={this.state.borders}
            />
          </Fragment> : 'Choose country'
        }

      </div>
    );
  }
}

export default CountryInfo;

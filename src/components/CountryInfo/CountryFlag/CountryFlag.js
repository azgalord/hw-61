import React from 'react';

import './CountryFlag.css';

const CountryFlag = props => {
  return (
    <div
      style={{backgroundImage: `url(${props.background})`}}
      className="CountryFlag"
    />
  );
};

export default CountryFlag;

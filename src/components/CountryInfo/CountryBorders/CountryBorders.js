import React from 'react';
import CountryBorder from "./CountryBorder/CountryBorder";

import './CountryBorders.css';

const CountryBorders = props => {
  return (
    <div className="CountryBorders">
      <h3>Borders with:</h3>
      <ul>
        {props.borders.map((border, id) => (
          <CountryBorder countryName={border} key={id}/>
        ))}
      </ul>
    </div>
  );
};

export default CountryBorders;

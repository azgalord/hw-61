import React from 'react';
import './CountryBorder.css';

const CountryBorder = props => {
  return (
    <li className="CountryBorder">{props.countryName}</li>
  );
};

export default CountryBorder;

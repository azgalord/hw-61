import React, {Component} from 'react';
import axios from 'axios';

import './Countries.css';
import Country from "./Country/Country";

class Countries extends Component {
  state ={
    countries: []
  };

  componentDidMount() {
    axios.get('/all?fields=name;alpha3Code')
      .then(response => {
        const countries = [];

        response.data.map(country => {
          return (
            countries.push(country)
          )
        });

        this.setState({countries});
      })
  }

  render() {
    return (
      <div className="Countries">
        {this.state.countries.map((country) => {
          return (
            <Country
              onClick={() => this.props.clicked(country.alpha3Code)}
              name={country.name}
              key={country.alpha3Code}
            />
          )
        })}
      </div>
    );
  }
}

export default Countries;

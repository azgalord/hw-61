import React from 'react';

import './Country.css';

const Country = props => {
  return (
    <p onClick={props.onClick} className="Country">{props.name}</p>
  );
};

export default Country;
